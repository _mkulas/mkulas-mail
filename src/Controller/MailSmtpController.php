<?php

namespace Mkulas\MailSmtp\Controller;

use App\Http\Controllers\Controller;
use Mkulas\MailSmtp\Repository\EmailData;
use Mkulas\MailSmtp\Services\Contracts\MailSettingContracts;
use Mkulas\MailSmtp\Statics\Email;

class MailSmtpController extends Controller
{

    public function __construct(
       private MailSettingContracts $mailSettings
    ) {
        //
    }

    public function index(): string
    {
       return view('mail::add');
    }

    public function getSmtp(): string
    {
        return $this->mailSettings->getEmail();
    }

    public function sendTestEmail(): void
    {
        Email::sendJob(EmailData::generate());
    }
}
