<?php

namespace Mkulas\MailSmtp;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\ServiceProvider;
use Mkulas\MailSmtp\Controller\MailSmtpController;
use Mkulas\MailSmtp\Services\Contracts\MailSettingContracts;
use Mkulas\MailSmtp\Services\MailSettingService;

class MailSmtpProvider extends ServiceProvider
{

    /**
     * @throws BindingResolutionException
     */
    public function register(): void
    {
        $this->registerServiceContracts();
        $this->app->make(MailSmtpController::class);
        $this->loadViewsFrom(__DIR__ . '/Views', 'mail');
    }

    public function boot(): void
    {
        $this->includeFile();

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../database/migrations/2022_01_05_173821_mail_settings.php.stub' => database_path('migrations/' . date('Y_m_d_His',
                        time()) . '_mail_settings.php'),
            ], 'migrations');
        }

    }

    private function includeFile(): void
    {
        include __DIR__ . '/routes.php';
    }

    private function registerServiceContracts(): void
    {
        $this->app->singleton(MailSettingContracts::class, MailSettingService::class);
    }
}
