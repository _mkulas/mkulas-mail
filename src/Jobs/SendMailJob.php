<?php

namespace Mkulas\MailSmtp\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Mkulas\MailSmtp\Mail\SendMail;

class SendMailJob implements ShouldQueue
{
    use Dispatchable, Queueable, SerializesModels;

    public function __construct(private $data)
    {
        //
    }

    public function handle(): void
    {
        Mail::send(new SendMail($this->data) );
    }
}
