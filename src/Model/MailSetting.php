<?php

namespace Mkulas\MailSmtp\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Mkulas\MailSmtp\Traits\UuidTrait;

/**
 * @property-read string $uuid
 * @property string $mail_host
 * @property integer $mail_port
 * @property string $mail_from_name
 * @property string $mail_user_password
 * @property string $mail_encrypt
 * @property string $mail_name
 * @property string $type
 * @property boolean $active
 * @method static inRandomOrder()
 * @method static query()
 * @method static whereUuid()
 */
class MailSetting extends Model
{
    use HasFactory, UuidTrait;

    protected static function boot(): void
    {
        parent::boot();
        self::creating( static function (self $model) {
            $model->mail_user_password = Crypt::encrypt($model->mail_user_password);
        });

        self::retrieved( static function ($model) {
            $model->mail_user_password = Crypt::decrypt($model->mail_user_password);
            return $model;
        });
    }

    protected $table = 'mail_settings';

    protected $hidden = ['id'];

    protected $fillable = [
        'user_id',
        'mail_host',
        'mail_port',
        'mail_from_name',
        'mail_user_password',
        'mail_encrypt',
        'mail_name',
        'type',
        'active'
    ];
}
