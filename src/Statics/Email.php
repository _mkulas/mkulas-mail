<?php

namespace Mkulas\MailSmtp\Statics;

use Illuminate\Foundation\Bus\PendingDispatch;
use JetBrains\PhpStorm\Pure;
use Mkulas\MailSmtp\Jobs\SendMailJob;
use Mkulas\MailSmtp\Model\MailSetting;
use Mkulas\MailSmtp\Services\MailSettingService;

class Email
{
    public static function sendJob($data): PendingDispatch
    {
        return SendMailJob::dispatch($data);
    }

    public static function send(string $to, string $emailName, string $subject, $emailData): int
    {
        return self::call()->send($to, $emailName, $subject, $emailData);
    }

    public static function getEmail(): string
    {
        return self::call()->getEmail();
    }

    public static function getMailSetting()
    {
        return self::call()->getMailSetting();
    }

    public static function defaultSetting(): object
    {
        return self::call()->defaultSetting();
    }

    public static function getAllMailSetting(): mixed
    {
        return self::call()->getAllMailSetting();
    }

    public static function getAuthUserAllSetting(): mixed
    {
        return self::call()->getAuthUserAllSetting();
    }

    public static function getSettingUuId($uuid): MailSetting
    {
        return self::call()->getSettingUuId($uuid);
    }

    public static function getActiveMailSetting()
    {
        return self::call()->getActiveMailSetting();
    }

    public static function deleteUserSmtp($uuid)
    {
        return self::call()->deleteUserSmtp($uuid);
    }

    #[Pure]
    private static function call(): MailSettingService
    {
        return (new MailSettingService());
    }
}
