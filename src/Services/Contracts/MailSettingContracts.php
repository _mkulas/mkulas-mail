<?php

namespace Mkulas\MailSmtp\Services\Contracts;

use Mkulas\MailSmtp\Model\MailSetting;

interface MailSettingContracts
{
    public function defaultSetting(): object;

    public function send(string $to, string $emailName, string $subject, $emailData): int;

    public function getMailSetting(): mixed;

    public function getActiveMailSetting(): mixed;

    public function getAllMailSetting(): mixed;

    public function getAuthUserAllSetting(): mixed;

    public function getSettingUuId($uuid): MailSetting;

    public function getEmail(): string;

    public static function deleteUserSmtp(string $uuid);

}
