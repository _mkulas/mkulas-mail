<?php

namespace Mkulas\MailSmtp\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Mkulas\MailSmtp\Model\MailSetting;
use Mkulas\MailSmtp\Services\Contracts\MailSettingContracts;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

final class MailSettingService implements MailSettingContracts
{
    public function defaultSetting(): object
    {
        return (object) [
            'mail_host' => config('mail.mailers.smtp.host'),
            'mail_port' => config('mail.mailers.smtp.port'),
            'mail_from_name' => config('mail.mailers.smtp.username'),
            'mail_user_password' => config('mail.mailers.smtp.password'),
            'mail_encrypt' => config('mail.mailers.smtp.encryption'),
            'mail_name' => config('mail.mailers.smtp.username')
        ];
    }


    public function send(string $to, string $emailName, string $subject, $emailData): int
    {
        $smtp = $this->configureSmtp();
        $mail_from = str_replace("\xE2\x80\x8B", "",$this->getEmail());
        $message = (new Swift_Message($subject))
            ->setFrom($mail_from, $emailName)
            ->setTo($to)
            ->setBody($emailData->render(),'text/html');

        return $smtp->send($message);
    }


    public function getMailSetting(): mixed
    {
        return MailSetting::query()->where('user_id', Auth::id())->first();
    }


    public function getActiveMailSetting(): mixed
    {
        return MailSetting::query()
            ->where('user_id', Auth::id())
            ->where('active',true)
            ->first();
    }


    public function getAllMailSetting(): mixed
    {
        return MailSetting::query()->get();
    }


    public function getAuthUserAllSetting(): mixed
    {
        return MailSetting::query()->where('user_id',Auth::id())->get();
    }


    public function getSettingUuId($uuid): MailSetting
    {
        return MailSetting::findOrFailByUuid($uuid);
    }


    public function getEmail(): string
    {
        $email = $this->getMailSetting();
        return $email
            ? $email->mail_from_name
            : config('mail.mailers.smtp.username');
    }


    public static function deleteUserSmtp(string $uuid): ?bool
    {
        return MailSetting::findOrFailByUuid($uuid)->delete();
    }


    private function configureSmtp(): Swift_Mailer
    {
        $get_setting = $this->getMailSetting();
        if (!empty($get_setting->mail_from_name)){
            $setting = $get_setting;
        } else {
            $setting = $this->defaultSetting();
        }

        $mail_crypt = str_replace("\xE2\x80\x8B", "",$setting->mail_encrypt);
        $transport = (new Swift_SmtpTransport($setting->mail_host, $setting->mail_port,$mail_crypt))
            ->setUsername($setting->mail_from_name)
            ->setPassword($setting->mail_user_password);
        return new Swift_Mailer($transport);
    }
}
