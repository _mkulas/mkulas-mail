<?php

use Illuminate\Support\Facades\Route;
use Mkulas\MailSmtp\Controller\MailSmtpController;

Route::get('mail',[MailSmtpController::class,'index']);
Route::get('mail-smtp',[MailSmtpController::class,'getSmtp']);
Route::get('mail-send',[MailSmtpController::class,'sendTestEmail']);
