<?php

namespace Mkulas\MailSmtp\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Mkulas\MailSmtp\Statics\Email;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(private $data)
    {
        //
    }

    public function build(): void
    {
        $mail = $this->to($this->data['to'], $this->setToName())
            ->from(Email::getEmail(), $this->setFromName())
            ->view($this->data['view'])
            ->with(['mailData' => $this->data['mailData']]);

        $this->setSubject($mail);
        $this->setCC($mail);
        $this->setBCC($mail);
        $this->setAttachment($mail);
    }

    private function setFromName(): string
    {
        return $this->data['from_name'] ?? Email::getEmail();
    }

    private function setToName(): string
    {
        return $this->data['to_name'] ?? $this->data['to'];
    }

    private function setSubject($mail): void
    {
        if (isset($this->data['subject'])) {
            $mail->subject($this->data['subject']);
        }
    }

    private function setAttachment($mail): void
    {
        if (isset($this->data['attachment'])) {
            foreach ($this->data['attachment'] as $file) {
                $mail->attach($file['path'], [
                    'as' => $file['as'],
                    'mime' => $file['mime']
                ]);
            }
        }
    }

    private function setCC($mail): void
    {
        if (isset($this->data['cc'])) {
            $mail->cc($this->data['cc']);
        }
    }

    private function setBCC($mail): void
    {
        if (isset($this->data['bcc'])) {
            $mail->bcc($this->data['bcc']);
        }
    }
}
