Install package    
        
    composer require mkulas/mail-smtp

if copy migration smtp settings

    php artisan vendor:publish --provider="Mkulas\MailSmtp\MailSmtpProvider" --tag="migrations"

Add crypt
        
    php artisan key:generate

Global method 
    
    use Mkulas\MailSmtp\Statics\Email;
    // will use Illuminate\Mail\Mailable;

    Email::sendJob($data)  // send email job parameters in 1.1
           getEmail() // return email .env or database if exist
           getMailSetting() // retrun mail setting db 
           defaultSetting() // return mail setting env
           getAllMailSetting() // retrun all setting email for admin
           getAuthUserAllSetting() // return all setting auth user
           getSettingUuId($uuid) // retrun setting where uuid
           getActiveMailSetting() // retrun setting in db if active = true
           deleteUserSmtp($uuid) // remove smtp setting


Parameters for send mail job

         $data = [
            'to' => 'test@email.com',
            'from_name' => 'name email',
            'to_name' => 'to name',
            'view' => 'mail', // view in resources 
            'mailData' => ['header' => 'data','message' => 'data'], // data for view ( array,string,compact() )
            'message' => string 'data', or array [data]  
            'subject' => 'name subject',
            'bcc' => ['test1@email.com','test2@email.com'],
            'bcc' => ['test3@email.com','test4@email.com'],
            'attachment' => [
                [
                    'mime' => '...',
                    'as' => '...',
                    'path' => '...',
                ]
            ]
        ];

        Email::sendJob($data);

Parameter for quick email  ( I do not recommend )

        use Mkulas\MailSmtp\Statics\Email;

        // will use Swift_Mailer;
        Email::send('test@mail.com','Name email','subject',view('mail',['data' => $data]));

